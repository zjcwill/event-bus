const EventEmitter  = require('./event')

const eventEmitter = new EventEmitter();

eventEmitter.addListener('on', name => {
  console.log('hello', name);
});

eventEmitter.emit('on', '小美');
