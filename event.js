class EventEmitter {
  constructor() {
    this._event = this._event || new Map();
    this._maxListeners = this._maxListeners || 10;
    this.emit = this.emit.bind(this);
    this.addListener = this.addListener.bind(this);
  }
  emit(type, ...args) {
    let handler;
    handler = this._event.get(type);
    if (args.length > 0) {
      handler.apply(this, args);
    } else {
      handler.call(this);
    }
    return true;
  }
  addListener(type, fn) {
    const handle = this._event.get(type);
    if (!handle) {
      this._event.set(type, fn);
    } else if (handle && typeof handle === 'function') {
      this._event.set(type, [handle, fn]);
    } else {
      handle.push(fn);
    }
  }
  removeListener(type, fn) {
    const handle = this._event.get(type);
    if (handle && typeof handle === 'function') {
      this._event.delete(type);
    } else {
      let position;
      for (let i = 0; i < handle.length; i++) {
        if (handle[i] === fn) {
          position = i;
        } else {
          position = -1;
        }
      }
      if (position !== -1) {
        handle.splice(position, 1);
        if (handle.length === 1) {
          this._event.set(type, handle[0]);
        }
      } else {
        return this;
      }
    }
  }
}

module.exports = EventEmitter;
